# Setup AWS Environment

For this guide, I am using an AWS instance (t2.micro) as host to my kubernetes cluster. Create the instance on the region that will host the kubernetes worker nodes. Go ahead and login to your AWS console.

## Create EC2 host

Go to EC2 page and Launch an instance with the following spec.

&nbsp;&nbsp;&nbsp;&nbsp;**Image**: Amazon Linux 2 AMI (HVM)<br />
&nbsp;&nbsp;&nbsp;&nbsp;**Type**: t2.micro<br />
&nbsp;&nbsp;&nbsp;&nbsp;**Instance Details**: [ Use defaults ]<br />
&nbsp;&nbsp;&nbsp;&nbsp;**Storage**: [ Use defaults ]<br />
&nbsp;&nbsp;&nbsp;&nbsp;**Tags**: [ Name: eks-host ]<br />
&nbsp;&nbsp;&nbsp;&nbsp;**Security group**: [ type: ssh, Source: Use My IP ]<br />

Launch the instance and wait for it to come up. Then, Using Putty, connect to your instance.

<%= image_tag "images/putty.png" %>

<aside class="notice">
To convert your pem file into ppk. refer to this guide: 
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html
</aside>

### Update awscli version
Before updating the awscli, check the version in the terminal.

```terminal
   [ec2-user@ip-172-31-0-105 ec2-user]# aws --version
   aws-cli/1.16.102 Python/2.7.14 Linux/4.14.123-111.109.amzn2.x86_64 botocore/1.12.92
```

<br />
The output version should be `1.16.156` or greater in order to run kubectl correctly. In the terminal, we have v1.16.102 and needs to be updated to latest version. Also, Your system's Python version must be 2.7.9 or greater. Otherwise, you receive hostname doesn't match errors with AWS CLI calls to Amazon EKS. 


```terminal
   [ec2-user@ip-172-31-0-105 ec2-user]# yum erase -y awscli
```

<%= image_tag "images/erase-awscli.gif" %>

Remove the awscli that is installed in the system. 

Install pip using easy_install command and then execute pip install awscli.

```terminal
   [ec2-user@ip-172-31-0-105 ec2-user]# easy_install pip
   [ec2-user@ip-172-31-0-105 ec2-user]# pip install awscli
   [ec2-user@ip-172-31-0-105 ec2-user]# aws --version
	aws-cli/1.16.185 Python/2.7.14 Linux/4.14.123-111.109.amzn2.x86_64 botocore/1.12.175
```

<%= image_tag "images/install-awscli.gif" %>

The output should show the latest version of awscli `1.16.185`.


### Install Kubectl

Kubernetes uses a command-line utility called kubectl for communicating with the cluster API server.

Follow the instructions on how to install kubectl v1.12. see
[https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)

<aside class="notice">
Amazon EKS latest supported version is v1.12, you can download the latest supported kubectl version but there might be some resources that is not yet supported in the cluster.
</aside>

```terminal
	[root@ip-172-31-0-105 ec2-user]# kubectl version --short --client
	Client Version: v1.12.9-eks-f1dbc0
```

## Create a IAM user

Create an IAM user with the following permissions and attach it to the instance.

* AmazonEC2FullAccess
* IAMFullAccess
* AmazonEKSClusterPolicy 
* AmazonEKSWorkerNodePolicy 
* AmazonEKSServicePolicy 
* AmazonEKS_CNI_Policy 

Add an inline policy and create a EKS policy and make sure to have full access.

## Create a IAM role

Create an IAM role with the following permissions for the instance to assume.

* AmazonEKSClusterPolicy 
* AmazonEKSServicePolicy 

Take note of the role arn value for use later. ex. arn:aws:iam::xxxxxxxxxx:role/<role name>

## Create VPC

Go to CloudFormation console and create a stack. Select the following:

In prepare template: `Template is ready`  <br />
Specify template: `Amazon S3 URL` <br />

In the specify URL field, input `https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/amazon-eks-vpc-sample.yaml` Fill in the parameters accordingly.

**Stack name**: eks-vpc-stack <br />
**VpcBlock**: `172.20.0.0/18` or keep the default value. <br />
**Subnet01Block**: `172.20.0.0/20` or keep the default value. <br />
**Subnet02Block**: `172.20.16.0/20` or keep the default value. <br />
**Subnet03Block**: `172.20.32.0/20` or keep the default value. <br />

Hit the create stack button and wait for cloudformation to complete the VPC stack. <br />

Once the status is `CREATE_COMPLETE`, go to the **Outputs tab** and take note of the following:

* SecurityGroups: sg-xxxxxxxxxxxxxx
* SubnetIds: subnet-xxxxxxxxxxxxx, subnet-xxxxxxxxxxxxx, subnet-xxxxxxxxxxxx
* VpcId: vpc-xxxxxxxxxxxxx

## Create the EKS Cluster

On host instance terminal, execute the following commands to create the cluster:

**cluster name**: eks-cloudbees-cluster
**region** ap-southeast-2

```terminal
	aws eks create-cluster --name eks-cloudbees-cluster \
	--role-arn arn:aws:iam::xxxxxxxxxxxx:role/<role name> \
	--resources-vpc-config \
	subnetIds=<subnet Ids>,securityGroupIds=<SecrurityGroups>
```


<%= image_tag "images/cluster-create.png" %>


Cluster creation will take about 10 - 20 minutes. Once the status is active, proceed to the next step.

## Connect to the cluster

Run the comand on the terminal. This will populate the kube config file on the users home directory ex. /root/.kube/config

```terminal
	aws eks --region ap-southeast-2 update-kubeconfig --name eks-cloudbees-cluster
```

<%= image_tag "images/cluster-connect.gif" %>

You can verify the connection by running the following command:

```terminal
	[root@ip-172-31-0-105 ec2-user]# kubectl get svc
```

<%= image_tag "images/cluster-verify.gif" %>

## Launch and Configure Amazon EKS Worker Nodes

Go to CloudFormation console and create a stack. Select the following:

In prepare template: `Template is ready`  <br />
Specify template: `Amazon S3 URL` <br />

In the specify URL field, input `https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/amazon-eks-nodegroup.yaml` Fill in the parameters accordingly.

**Stack name**: eks-cloudbees-cluster-worker-nodes<br />
**ClusterName**: eks-cloudbees-cluster<br />
**ClusterControlPlaneSecurityGroup**: { SecurityGroups } * <br />
**NodeGroupName**: eks-worker<br />
**NodeAutoScalingGroupMinSize**: 3 \*\*<br />
**NodeAutoScalingGroupDesiredCapacity**: 3 \*\*<br />
**NodeAutoScalingGroupMaxSize**: 3 \*\*<br />
**NodeInstanceType**: t3.medium<br />
**NodeImageId**: ami-01bfe815f644becc0<br />
**KeyName**: { enter a pem key }<br />
**VpcId**: { VpcId } \*\*\*<br />
**SubnetIds**": { SubnetIds } \*\*\*<br />

\* Choose the SecurityGroups value from the AWS CloudFormation output that you generated earlier.<br />
\*\* Enter the minimum, desired and maximum number of nodes that your worker node Auto Scaling group can scale in to.<br />
\*\*\*Choose the VpcId and subnetIds value from the AWS CloudFormation output that you generated earlier.<br />

Click Next then in Tags, add `Name: eks-worker` then check the acknowledge IAM:Role permission below. <br />

Take note of the following information in the outputs tab.

**NodeInstanceRole**: arn:aws:iam::xxxxxxxxxxxx:role/eks-cloudbees-cluster-worker-NodeInstanceRole-xxxxxxxxxxxxx <br />
**NodeSecurityGroup**: sg-xxxxxxxxxxxxxxxx <br />
 
Once the status is complete, proceed to the next step.

## To enable worker nodes to join your cluster

Download the `aws-auth-cm.yaml` file and modify using vi editor.

```terminal
	curl -o aws-auth-cm.yaml \
	https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-02-11/aws-auth-cm.yaml
```

<%= image_tag "images/join-worker-to-cluster.gif" %>

copy the *NodeInstanceRole* value and replace it to the `<ARN of instance role (not instance profile)>` of aws-auth-cm.yaml.

```terminal
	kubectl apply -f aws-auth-cm.yaml
```

<%= image_tag "images/join-worker-to-cluster2.gif" %>

Verify that the nodes are connected to the cluster.

```terminal
	[root@ip-172-31-0-105 ec2-user]# kubectl get nodes
	NAME                                               STATUS   ROLES    AGE   VERSION
	ip-172-20-15-252.ap-southeast-2.compute.internal   Ready    <none>   76s   v1.12.7
	ip-172-20-18-116.ap-southeast-2.compute.internal   Ready    <none>   75s   v1.12.7
	ip-172-20-38-58.ap-southeast-2.compute.internal    Ready    <none>   81s   v1.12.7
```

Congrats! you now have a working cluster in Amazon EKS.










<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />









	